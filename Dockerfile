FROM python:3.9-slim-buster

ARG DJANGO_ENV

# Port used by this container to serve HTTP.
EXPOSE 8000

ENV DJANGO_ENV=${DJANGO_ENV} \
  PYTHONFAULTHANDLER=1 \
  PYTHONUNBUFFERED=1 \
  PYTHONHASHSEED=random \
  PIP_NO_CACHE_DIR=off \
  PIP_DISABLE_PIP_VERSION_CHECK=on \
  PIP_DEFAULT_TIMEOUT=100 \
  POETRY_VERSION=1.1.12 \
  PORT=8000

RUN apt-get update \
    && apt-get -y install postgresql libpq-dev gcc

# System deps:
RUN pip install "poetry==$POETRY_VERSION"

# Install the application server.
RUN pip install "gunicorn==20.0.4"

# Copy only requirements to cache them in docker layer
WORKDIR /app

# Add user and group
RUN groupadd -r web && useradd -d /app -r -g web web \
  && chown web:web -R /app

USER web

COPY --chown=web:web poetry.lock pyproject.toml /app/

# Project initialization:
RUN poetry config virtualenvs.create false \
  && poetry install $(test "$DJANGO_ENV" == production && echo "--no-dev") --no-interaction --no-ansi

# Creating folders, and files for a project:
COPY --chown=web:web . /app

# Collect static files.
RUN poetry run python manage.py collectstatic --noinput --clear

CMD poetry run python manage.py migrate --noinput; gunicorn config.wsgi:application

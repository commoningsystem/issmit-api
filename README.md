# issmit-backend

This is the backend for the issmit.app project. It is using Python, Django and the Django REST Framework to provide an API.

## Technologies

- Python and the [Django web framework](https://www.djangoproject.com)
- [Django REST framework](https://www.django-rest-framework.org) for the api part
- OAuth2 for authentication using the [Django OAuth Toolkit](https://django-oauth-toolkit.readthedocs.io/en/latest/)

## Setup

### Development

This project uses [Poetry](https://python-poetry.org) for package management and setting up a virtual environment.

Requirements:

  - Python 3.5+
  - [Install Poetry](https://python-poetry.org/docs/)

Run `poetry install` inside the root directory of the project.

Use `poetry shell` to activate the virtual environment or use `poetry run` to run a command inside the virtual environment.

Run migrations: `poetry run python manage.py migrate`

Create a superuser: `poetry run python manage.py createsuperuser`

Start the server: `poetry run python manage.py runserver`

### Production environment with docker

The provided docker-compose file sets up a environment with Django and Postgres DB.

Generate a secret key for django:

``python -c "import secrets; print(secrets.token_urlsafe())"``

Build and run:

``SECRET_KEY='<secret key>' docker-compose up --build``

While previous docker-compose command is still running run the migrations:

``SECRET_KEY='<secret key>' docker-compose run app poetry run python manage.py migrate``

And create a superuser:

``SECRET_KEY='<secret key>' docker-compose run app poetry run python manage.py createsuperuser``

You can now open http://localhost:8000/admin and log into the backend.

### OAuth2

For OIDC features to work a RSA key needs to be generated and saved with the file name ``oidc.key`` to the root directory:

``openssl genrsa -out oidc.key 4096``

After starting the backend you need to add a application in the admin backend.

  - Redirect uris: Set this to the base URL of the frontend, e.g. ``http://localhost:8080/``
  - Client type: Public
  - Authorization grant type: Authorization code
  - Algorithm: RSA

## API endpoints

**``/o/*``**

OAuth2 routes are available on ``/o``

**``GET /api/v1/events``**

Returns a list of all events

**``POST /api/v1/events``**

Creates an event

**``GET /api/v1/events/<id>/``**

Returns event with id <id>

**``GET /api/v1/process_specifcations``**

Returns a list of all process specifications

**``POST /api/v1/process_specifcations``**

Creates a process specifications

**``GET /api/v1/process_specifcations/<id>/``**

Returns process specifications with id <id>

**``GET /api/v1/recipe_processes``**

Returns a list of all recipe processes

**``POST /api/v1/recipe_processes``**

Creates a recipe process

**``GET /api/v1/recipe_processes/<id>/``**

Returns recipe process with id <id>

**``GET /api/v1/resource_specifications``**

Returns a list of all resource specifications

**``POST /api/v1/resource_specifications``**

Creates a resource specification

**``GET /api/v1/resource_specifications/<id>/``**

Returns resource specification with id <id>

**``GET /api/v1/recipe_resources``**

Returns a list of all recipe resources

**``POST /api/v1/recipe_resources``**

Creates a recipe resource

**``GET /api/v1/recipe_resources/<id>/``**

Returns recipe resource with id <id>

**``GET /api/v1/recipe_flow``**

Returns a list of all recipe flow

**``POST /api/v1/recipe_flow``**

Creates a recipe flow

**``GET /api/v1/recipe_flow/<id>/``**

Returns recipe flow with id <id>

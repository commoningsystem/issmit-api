from rest_framework import serializers

from django.contrib.auth.models import User

from issmit.models import (
    Event,
    ProcessSpecification,
    RecipeFlow,
    RecipeProcess,
    RecipeResource,
    ResourceSpecification,
)


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("username", "email", "first_name", "last_name")


class EventSerializer(serializers.ModelSerializer):
    class Meta:
        model = Event
        fields = (
            "id",
            "takes_place_on",
            "takes_place_at",
            "pattern",
            "name",
            "description",
            "host",
            "banner",
            "max_participants_count",
            "max_local_participants_count",
            "max_pickup_participants_count",
            "pickup_time",
        )
        depth = 1


class ProcessSpecificationSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProcessSpecification
        fields = "__all__"


class RecipeProcessSerializer(serializers.ModelSerializer):
    class Meta:
        model = RecipeProcess
        fields = "__all__"
        depth = 1


class ResourceSpecificationSerializer(serializers.ModelSerializer):
    class Meta:
        model = ResourceSpecification
        fields = "__all__"


class RecipeResourceSerializer(serializers.ModelSerializer):
    class Meta:
        model = RecipeResource
        fields = "__all__"
        depth = 1


class RecipeFlowSerializer(serializers.ModelSerializer):
    class Meta:
        model = RecipeFlow
        fields = "__all__"
        depth = 1

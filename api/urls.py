from django.urls import path
from .views import (
    ProcessSpecificationDetails,
    RecipeFlowDetails,
    RecipeFlowListCreate,
    ProcessSpecificationListCreate,
    RecipeProcessDetails,
    RecipeProcessListCreate,
    RecipeResourceDetails,
    RecipeResourceListCreate,
    ResourceSpecificationDetails,
    ResourceSpecificationListCreate,
    UserList,
    UserDetails,
    EventsListCreate,
    EventDetails,
)

urlpatterns = [
    path("users/", UserList.as_view()),
    path("users/<pk>/", UserDetails.as_view()),
    path("events/", EventsListCreate.as_view()),
    path("events/<pk>/", EventDetails.as_view()),
    path("process_specifications/", ProcessSpecificationListCreate.as_view()),
    path("process_specifications/<pk>/", ProcessSpecificationDetails.as_view()),
    path("recipe_processes/", RecipeProcessListCreate.as_view()),
    path("recipe_processes/<pk>/", RecipeProcessDetails.as_view()),
    path("resource_specifications/", ResourceSpecificationListCreate.as_view()),
    path("resource_specifications/<pk>/", ResourceSpecificationDetails.as_view()),
    path("recipe_resources/", RecipeResourceListCreate.as_view()),
    path("recipe_resources/<pk>/", RecipeResourceDetails.as_view()),
    path("recipe_flows/", RecipeFlowListCreate.as_view()),
    path("recipe_flows/<pk>/", RecipeFlowDetails.as_view()),
]

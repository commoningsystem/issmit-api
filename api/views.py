from rest_framework import generics, permissions

from django.contrib.auth.models import User
from issmit.models import (
    Event,
    ProcessSpecification,
    RecipeProcess,
    RecipeResource,
    ResourceSpecification,
    RecipeFlow,
)
from .serializers import (
    ProcessSpecificationSerializer,
    RecipeProcessSerializer,
    RecipeResourceSerializer,
    ResourceSpecificationSerializer,
    RecipeFlowSerializer,
    UserSerializer,
    EventSerializer,
)

from oauth2_provider.contrib.rest_framework import TokenHasReadWriteScope, TokenHasScope


class UserList(generics.ListCreateAPIView):
    permission_classes = [permissions.IsAuthenticated, TokenHasReadWriteScope]
    queryset = User.objects.all()
    serializer_class = UserSerializer


class UserDetails(generics.RetrieveAPIView):
    permission_classes = [permissions.IsAuthenticated, TokenHasReadWriteScope]
    queryset = User.objects.all()
    serializer_class = UserSerializer


class EventsListCreate(generics.ListCreateAPIView):
    # permission_classes = [permissions.IsAuthenticated, TokenHasScope]
    # required_scopes = ['events']
    queryset = Event.objects.all()
    serializer_class = EventSerializer


class EventDetails(generics.RetrieveAPIView):
    queryset = Event.objects.all()
    serializer_class = EventSerializer


class ProcessSpecificationListCreate(generics.ListCreateAPIView):
    queryset = ProcessSpecification.objects.all()
    serializer_class = ProcessSpecificationSerializer


class ProcessSpecificationDetails(generics.RetrieveAPIView):
    queryset = ProcessSpecification.objects.all()
    serializer_class = ProcessSpecificationSerializer


class RecipeProcessListCreate(generics.ListCreateAPIView):
    queryset = RecipeProcess.objects.all()
    serializer_class = RecipeProcessSerializer


class RecipeProcessDetails(generics.RetrieveAPIView):
    queryset = RecipeProcess.objects.all()
    serializer_class = RecipeProcessSerializer


class ResourceSpecificationListCreate(generics.ListCreateAPIView):
    queryset = ResourceSpecification.objects.all()
    serializer_class = ResourceSpecificationSerializer


class ResourceSpecificationDetails(generics.RetrieveAPIView):
    queryset = ResourceSpecification.objects.all()
    serializer_class = ResourceSpecificationSerializer


class RecipeResourceListCreate(generics.ListCreateAPIView):
    queryset = RecipeResource.objects.all()
    serializer_class = RecipeResourceSerializer


class RecipeResourceDetails(generics.RetrieveAPIView):
    queryset = RecipeResource.objects.all()
    serializer_class = RecipeResourceSerializer


class RecipeFlowListCreate(generics.ListCreateAPIView):
    queryset = RecipeFlow.objects.all()
    serializer_class = RecipeFlowSerializer


class RecipeFlowDetails(generics.RetrieveAPIView):
    queryset = RecipeFlow.objects.all()
    serializer_class = RecipeFlowSerializer

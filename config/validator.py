from oauth2_provider.oauth2_validators import OAuth2Validator


class IssmitOAuth2Validator(OAuth2Validator):
    def get_userinfo_claims(self, request):
        claims = super().get_userinfo_claims(request)
        claims["username"] = request.user.get_full_name()
        return claims

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User
from .models import (
    Person,
    ProcessSpecification,
    RecipeProcess,
    ResourceSpecification,
    RecipeResource,
    RecipeFlow,
    Event,
    Contribution,
    EventParticipation,
)


class PersonInline(admin.StackedInline):
    model = Person
    can_delete = False
    verbose_name_plural = "people"


class UserAdmin(BaseUserAdmin):
    inlines = (PersonInline,)


class RecipeFlowAdmin(admin.ModelAdmin):
    list_display = ("__str__", "resource_quantity")


class ContributionInline(admin.TabularInline):
    model = Contribution
    extra = 1


class EventParticipationInline(admin.TabularInline):
    model = EventParticipation
    extra = 1


class EventAdmin(admin.ModelAdmin):
    inlines = (ContributionInline, EventParticipationInline)


admin.site.unregister(User)
admin.site.register(User, UserAdmin)
admin.site.register(ProcessSpecification)
admin.site.register(RecipeProcess)
admin.site.register(ResourceSpecification)
admin.site.register(RecipeResource)
admin.site.register(RecipeFlow, RecipeFlowAdmin)
admin.site.register(Event, EventAdmin)

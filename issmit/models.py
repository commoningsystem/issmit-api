from django.db import models
from django.contrib.auth.models import User


class Person(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    image = models.ImageField(blank=True, null=True)
    primary_location = models.CharField(max_length=250, blank=True)


# Knowledge


class ProcessSpecification(models.Model):
    name = models.CharField(max_length=250)

    def __str__(self):
        return self.name


class RecipeProcess(models.Model):
    process_conforms_to = models.ForeignKey(
        ProcessSpecification, on_delete=models.CASCADE
    )
    name = models.CharField(max_length=250, blank=True)

    def __str__(self):
        if self.name:
            return self.name
        return "{process} (Generic recipe {id})".format(
            process=self.process_conforms_to.name, id=self.pk
        )


class ResourceSpecification(models.Model):
    name = models.CharField(max_length=250)
    image = models.ImageField(blank=True, null=True)
    default_unit_of_resource = models.CharField(max_length=250)

    def __str__(self):
        return self.name


class RecipeResource(models.Model):
    resource_conforms_to = models.ForeignKey(
        ResourceSpecification, on_delete=models.CASCADE
    )
    unit_of_resource = models.CharField(max_length=250)

    def __str__(self):
        return self.resource_conforms_to.name


class RecipeFlow(models.Model):
    recipe_flow_resource = models.ForeignKey(RecipeResource, on_delete=models.CASCADE)
    recipe_input_of = models.ForeignKey(RecipeProcess, on_delete=models.CASCADE)
    resource_quantity = models.IntegerField()

    def __str__(self):
        return "{resource} in {recipe}".format(
            resource=self.recipe_flow_resource, recipe=self.recipe_input_of
        )


# Planning


class Event(models.Model):
    takes_place_on = models.DateTimeField(blank=True, null=True)
    takes_place_at = models.CharField(
        max_length=250, blank=True
    )  # TODO: how do we want to represent locations?
    pattern = models.ForeignKey(
        ProcessSpecification, null=True, blank=True, on_delete=models.SET_NULL
    )
    name = models.CharField(max_length=250)
    description = models.TextField(blank=True)
    banner = models.ImageField(blank=True, null=True)
    host = models.ForeignKey(
        User, null=True, blank=True, on_delete=models.SET_NULL, related_name="hosting"
    )
    max_participants_count = models.IntegerField(blank=True, null=True)
    max_local_participants_count = models.IntegerField(blank=True, null=True)
    max_pickup_participants_count = models.IntegerField(blank=True, null=True)
    pickup_time = models.DateTimeField(blank=True, null=True)
    contributors = models.ManyToManyField(
        User, through="Contribution", related_name="contributing"
    )
    participants = models.ManyToManyField(
        User, through="EventParticipation", related_name="participating"
    )

    def __str__(self):
        return self.name


class Contribution(models.Model):
    contributor = models.ForeignKey(User, on_delete=models.CASCADE)
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    recipe_flow_resource = models.ForeignKey(RecipeResource, on_delete=models.CASCADE)
    resource_quantity = models.IntegerField()


class EventParticipation(models.Model):
    MODE_CHOICES = [("partake", "Take part in event"), ("pickup", "Only pickup")]
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    participant = models.ForeignKey(User, on_delete=models.CASCADE)
    is_committed = models.BooleanField()
    mode = models.CharField(max_length=10, choices=MODE_CHOICES, default="partake")
